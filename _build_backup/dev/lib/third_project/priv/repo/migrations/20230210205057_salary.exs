defmodule ThirdProject.Repo.Migrations.Salary do
  use Ecto.Migration

  def change do
    create table(:salarys) do
      add :amount, :string
      add :issue_date, :string
      add :user_id, references(:users)
    end
  end
end
