defmodule ThirdProject.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :age, :string
      add :dob, :string
      add :gender, :string
      add :country, :string
    end
  end
end
