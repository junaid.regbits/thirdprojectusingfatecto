defmodule ThirdProject.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :name, :string
    field :age, :string
    field :dob, :string
    field :gender, :string
    field :country, :string
    has_many :salary, ThirdProject.Accounts.Salary
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
