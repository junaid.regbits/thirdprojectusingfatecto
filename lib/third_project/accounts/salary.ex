defmodule ThirdProject.Accounts.Salary do
  use Ecto.Schema
  import Ecto.Changeset

  schema "salarys" do
    field :amount, :string
    field :issue_date, :string
    belongs_to :user, ThirdProject.Accounts.User
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:amount])
    |> validate_required([:amount])
  end
end
