defmodule ThirdProject.Repo do
  use Ecto.Repo,
    otp_app: :third_project,
    adapter: Ecto.Adapters.Postgres
end
