defmodule ThirdProject.Query do
  use FatEcto.FatQuery, otp_app: :third_project, max_limit: 103, default_limit: 34, repo: ThirdProject.Repo
end
