defmodule ThirdProject.Accounts do
  import Ecto.Query, warn: false
  import ThirdProject.Query
  alias ThirdProject.Repo
  alias ThirdProject.Accounts.User
  # alias ThirdProject.Accounts.Salary

  def filter_user() do
    query_opts = %{"$where" => %{"name" => "junaid"}}
    User
    |> build!(query_opts)
    |> Repo.all()
  end
  def wrong_fields() do
    query_opts = %{"$where" => %{"somethingelse" => "junaid"}}
    User
    |> build!(query_opts)
    |> Repo.all()
  end


  def multiple_fields() do
    query_opts = %{
     "$where" => %{"name" => "junaid", "age" => "25"}
     }
    User
    |> build!(query_opts)
    |> Repo.all()
  end

  def multiple_fields_with_wrong_data() do
    query_opts = %{
      "$where" => %{"name" => "junaid", "something" => "25"}
      }
    User
    |> build!(query_opts)
    |> Repo.all()
  end

  def multiple_schemas() do
    query_opts = %{
      "$where" => %{"name" => "junaid"},
      "$include" => %{"salary" => %{"$where" => %{"amount" => "20000"}}}
                    }
    User
    |> build!(query_opts)
    |> Repo.all()
  end

  def list_users do
    Repo.all(User)
  end
  def get_user!(name), do: Repo.get_by(User, name: name) |> Repo.preload(:salary)
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end
def delete_user(%User{} = user) do
    Repo.delete(user)
  end
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end
end
