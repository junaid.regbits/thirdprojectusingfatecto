defmodule ThirdProjectWeb.PageController do
  use ThirdProjectWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
