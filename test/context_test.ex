defmodule Fat.ContextTest do
  use ThirdProjectWeb.ConnCase
  alias Fat.ContextMacro
  alias ThirdProject.{Repo, Accounts.User}

  setup do
    Repo.start_link()
    Repo.insert(%User{name: "junaid"})
    :ok
  end

  # test "return first record from table" do
  #   Repo.insert(%User{name: "junaid", age: "24"})

  #   room =
  #     from(c in User,
  #       where: c.name == "Junaid",
  #       limit: 1
  #     )
  #     |> Repo.one()


  #   record = ContextMacro.first(FatEcto.FatRoom, [:fat_beds])
  #   assert record.name == "John"
  #   sibling = List.first(record.fat_beds)
  #   assert sibling.fat_room_id == room.id
  # end

  test "get record" do
    {:ok, user} = Repo.insert(%User{name: "junaid"})

    record = ContextMacro.get!(User, user.id)
    assert record.id == user.id
  end

end
