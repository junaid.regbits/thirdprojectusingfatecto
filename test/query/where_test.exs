defmodule Query.WhereTest do
  use ThirdProjectWeb.ConnCase
  # import FatEcto.TestRecordUtils
  alias ThirdProject.Repo
  import Ecto.Query

  # setup do
  #   Repo.insert(:user)
  #   :ok
  # end

test "returns the query where fields name are allowed" do
  opts = %{"$where" => %{"name" => "junaid"}}
  expected = from(h in ThirdProject.User, where: h.name == ^"junaid" and ^true)
  query = ThirdProject.Query.build!(ThirdProject.User, opts)
  assert inspect(query) == inspect(expected)
  assert Repo.one(query) == nil
end

test "returns the query where location field will be ignored" do
  opts = %{"$where" => %{"name" => "junaid", "location" => "something"}}
  expected = from(h in ThirdProject.User, where: h.name == ^"junaid" and ^true)
  query = ThirdProject.Query.build!(ThirdProject.User, opts)
  assert inspect(query) == inspect(expected)
  assert Repo.one(query) == nil
end
test "returns the query where both fields are allowed" do
  opts = %{"$where" => %{"name" => "junaid", "age" => "25"}}
  expected = from(h in ThirdProject.User, where: h.name == ^"junaid" and (h.age == ^"25" and ^true))
  query = ThirdProject.Query.build!(ThirdProject.User, opts)
  assert inspect(query) == inspect(expected)
  assert Repo.one(query) == nil
end
test "returns the query where both fields will ignored" do
  opts = %{"$where" => %{"lname" => "junaid", "location" => "something"}}
  expected = from(h in ThirdProject.User)
  query = ThirdProject.Query.build!(ThirdProject.User, opts)
  assert inspect(query) == inspect(expected)
  assert Repo.one(query) == nil
end
end
