defmodule ThirdProject.MyAppQuery do
  use FatEcto.FatQuery,
    otp_app: :third_project,
    max_limit: 103,
    default_limit: 34,
    blacklist_params: [
      {:user, ["location"]}
    ],
    repo: ThirdProject.Repo
end
